# MCAI Docker

This crate is a wrapper on bollard crate to enable starting a MCAI worker with Docker.

## Usage

```rust
use bollard::Docker;
use mcai_docker::McaiDocker;
use std::io::stdout;

#[tokio::main]
async fn main() {
  let docker = Docker::connect_with_socket_defaults().unwrap();
  let mcai_docker = McaiDocker::new(docker);
  mcai_docker
    .run_worker(
      "mcai_worker_image_name",
      &[("KEY1", Box::new("VALUE1"))],
      Some(&mut stdout()),
    )
    .await
    .unwrap();
}
```
