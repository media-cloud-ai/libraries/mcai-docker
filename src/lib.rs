pub mod container;
pub mod error;
pub mod image;

pub use bollard::{auth::DockerCredentials, Docker};

use crate::error::Result;
use crate::image::Image;
use std::io::Write;

pub struct McaiDocker {
  credentials: Option<DockerCredentials>,
  docker: Docker,
}

pub struct ConfigOptions {}

impl McaiDocker {
  pub fn new(docker: Docker) -> Self {
    McaiDocker {
      docker,
      credentials: None,
    }
  }

  pub fn with_credential(mut self, credentials: DockerCredentials) -> Self {
    self.credentials = Some(credentials);
    self
  }

  pub async fn describe_worker(
    &self,
    docker_image_name: &str,
    stream: Option<&mut (dyn Write + Send)>,
  ) -> Result<()> {
    let builder = Image::new(docker_image_name)
      .with_optional_credentials(self.credentials.clone())
      .build(&self.docker)
      .await?;
    let container = builder.with_env("DESCRIBE", &1).build(&self.docker).await?;
    container.run(&self.docker, stream).await?;
    container.remove(&self.docker, true).await
  }

  pub async fn run_worker_with_source_orders(
    &self,
    docker_image_name: &str,
    source_orders: &[&str],
    stream: Option<&mut (dyn Write + Send)>,
  ) -> Result<()> {
    self
      .run_worker(
        docker_image_name,
        &[("SOURCE_ORDERS", &source_orders.join(":"))],
        stream,
      )
      .await
  }

  pub async fn run_worker(
    &self,
    docker_image_name: &str,
    envs: &[(&str, &(dyn ToString + Send + Sync))],
    stream: Option<&mut (dyn Write + Send)>,
  ) -> Result<()> {
    let builder = Image::new(docker_image_name)
      .with_optional_credentials(self.credentials.clone())
      .build(&self.docker)
      .await?;

    let container = builder
      .with_enable_tty()
      .with_envs(envs)
      .build(&self.docker)
      .await?;
    container.run(&self.docker, stream).await?;
    container.remove(&self.docker, true).await
  }
}
