use std::fmt::{self, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
  Docker(bollard::errors::Error),
  File(std::io::Error),
}

impl From<bollard::errors::Error> for Error {
  fn from(error: bollard::errors::Error) -> Self {
    Error::Docker(error)
  }
}

impl From<std::io::Error> for Error {
  fn from(error: std::io::Error) -> Self {
    Error::File(error)
  }
}

impl Display for Error {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    match self {
      Error::Docker(error) => write!(f, "Docker: {:?}", error),
      Error::File(error) => write!(f, "File: {:?}", error),
    }
  }
}
